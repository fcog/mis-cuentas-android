package com.franciscogiraldo.fcog.gastos.utils;

/**
 * Created by fcog on 9/7/15.
 */
    public class Constantes {
        /**
         * Puerto que utilizas para la conexión.
         * Dejalo en blanco si no has configurado esta característica.
         */
        private static final String PUERTO_HOST = ":80";

        /**
         * Dirección IP de genymotion o AVD
         */
        private static final String IP = "http://199.195.116.112";

        /**
         * URLs del Web Service
         */
        public static final String GET_URL = IP + PUERTO_HOST + "/FCOG/gastos/obtener_gastos.php";
        public static final String INSERT_URL = IP + PUERTO_HOST + "/FCOG/gastos/insertar_gasto.php";

        /**
         * Campos de las respuestas Json
         */
        public static final String ID_GASTO = "idGasto";
        public static final String ESTADO = "estado";
        public static final String GASTOS = "gastos";
        public static final String MENSAJE = "mensaje";

        /**
         * Códigos del campo {@link ESTADO}
         */
        public static final String SUCCESS = "1";
        public static final String FAILED = "2";

        /**
         * Tipo de cuenta para la sincronización
         */
        public static final String ACCOUNT_TYPE = "com.franciscogiraldo.gastos.account";
    }
